// BÀI 1:CHO NGƯỜI DÙNG NHẬP VÀO 3 SỐ NGUYÊN, VIẾT CHƯƠNG TRÌNH NHẬP 3 SỐ THEO THỨ TỰ TĂNG DẦN

function sapXep() {
  var a = document.getElementById("txt-so-thu-1").value * 1;
  var b = document.getElementById("txt-so-thu-2").value * 1;
  var c = document.getElementById("txt-so-thu-3").value * 1;

  var result;

  if (a > b && b > c) {
    result = c + "<" + b + "<" + a;
  } else if (a > c && c > b) {
    result = b + "<" + c + "<" + a;
  } else if (b > a && a > c) {
    result = c + "<" + a + "<" + b;
  } else if (b > c && c > a) {
    result = a + "<" + c + "<" + b;
  } else if (c > a && a > b) {
    result = b + "<" + a + "<" + c;
  } else {
    result = a + "<" + b + "<" + c;
  }

  document.getElementById("result1").innerHTML = `<h1> ${result}</h1>`;
  result1.style.background = "#E62B4A";
}

// BÀI 2: VIẾT CHƯƠNG TRÌNH "CHÀO HỎI" CÁC THÀNH VIÊN TRONG GIA ĐÌNH

function greetingByUsername() {
  var userName = document.getElementById("txt-choose").value;
  var family = greetingfamily(userName);
  document.getElementById("result2").innerHTML = `<p>${family}</p>`;
  result2.style.background = "#0D3ECC";
}
function greetingfamily(name) {
  switch (name) {
    case "0": {
      return getgreeting("Ba");
    }
    case "1": {
      return getgreeting("Mẹ");
    }
    case "2": {
      return getgreeting("Anh Trai");
    }
    case "3": {
      return getgreeting("Em Gái");
    }
  }
}
function getgreeting(username) {
  return `Xin Chào ${username}`;
}

// BÀI 3: ĐẾM SỐ CHẲN LẺ
function check() {
  var num1 = document.getElementById("txt-num1").value * 1;
  var num2 = document.getElementById("txt-num2").value * 1;
  var num3 = document.getElementById("txt-num3").value * 1;
  var countEven = "0";

  if (num1 % 2 == 0) {
    countEven++;
  }
  if (num2 % 2 == 0) {
    countEven++;
  }
  if (num3 % 2 == 0) {
    countEven++;
  }

  var countOdd = 3 - countEven;

  document.getElementById(
    "result3"
  ).innerHTML = `<p> Có ${countEven} chẳn và có ${countOdd} lẻ </p>`;
  result3.style.background = "#E62B4A";
}

// BÀI 4: ĐOÁN HÌNH TAM GIÁC
function guess() {
  var length1 = document.getElementById("txt-length1").value * 1;
  var length2 = document.getElementById("txt-length2").value * 1;
  var length3 = document.getElementById("txt-length3").value * 1;
  var length11 = Math.sqrt(length2 * length2 + length3 * length3);
  var length22 = Math.sqrt(length1 * length1 + length3 * length3);
  var length33 = Math.sqrt(length2 * length2 + length1 * length1);
  var result;

  if (length1 == length2 && length2 == length3 && length1 > 0 && length2 > 0 && length3 > 0) {
    result = "Tam Giác Đều";
    console.log("result: ", result);
  } else if (length1 == length2 || length1 == length3 || length2 == length3 && length1 > 0 && length2 > 0 && length3 > 0) {
    result = "Tam Giác Cân";
    console.log("result: ", result);
  } else if (
    length1 == length11 ||
    length2 == length22 ||
    length3 == length33
  ) {
    result = "Tam Giác Vuông";
    console.log("result: ", result);
  } else {
    result = "Tam Giác Nào Đó Chưa Xác Định";
    console.log("result: ", result);
  }

  document.getElementById("result4").innerHTML = `<p>Đây là ${result}</p>`;
  result4.style.background = "#F8E1DB";
}
